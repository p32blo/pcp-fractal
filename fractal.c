/*
 * Trabalho 2 - Fractal de Mandelbrot Hibrido
 * André Oliveira - pg27770
 */

/* Includes */

#include <stdio.h>
#include <stdlib.h>

#include <omp.h>
#include <mpi.h>

/* Macro Defines */

#define SIZE 2048
#define MAX_ITER 50

#define AT(vec, i, j) vec[((i) * (SIZE) + (j))]

/* Initial Conditions */

float c0r = -2.05f;
float c0i = -2.05f;

float side = 4.1f;
float scale; // side / img_size;

int x0 = 0;
int y0 = 0;

int comp_point(float cr, float ci)
{
    float zr = 0.0f;
    float zi = 0.0f;
    int count = 0;

    while ((count < MAX_ITER) && ((zr * zr) + (zi * zi) < 4.0f)) {
        float new_zr = (zr * zr) - (zi * zi) + cr;
        zi = 2.0f * zr * zi + ci;
        zr = new_zr;
        count++;
    }
    return count;
}

void mandel_brot(int pict[], int start, int end)
{
	#pragma omp parallel for schedule(dynamic)
	for (int y = y0 + start; y < y0 + end; ++y) {
        float ci = c0i + y * scale;
		for (int x = x0; x < x0 + SIZE; ++x) {
            float cr = c0r + x * scale;
			AT(pict, y - y0 - start, x - x0) = comp_point(cr, ci);
        }
    }
}

void write_pic(int pict[], char* filename)
{
	FILE* f = fopen(filename, "w");

    if (!f) {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    fprintf(f, "P2\n");
	fprintf(f, "%d %d\n%d\n", SIZE, SIZE, MAX_ITER - 1);
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
			fprintf(f, "%d ", AT(pict, i, j) - 1);
        }
    }

    fclose(f);
    fprintf(stdout, "'%s' saved\n", filename);
}

void print_pic(int pict[])
{
	printf("w:%d h:%d\n", SIZE, SIZE);
	for (int i = 0; i < SIZE; ++i) {
		for (int j = 0; j < SIZE; ++j) {
			printf("%d ", AT(pict, i, j));
		}
		printf("\n");
	}
}

void print_vec(int vec[], int size)
{
	printf("size: %d -> ", size);
	for (int i = 0; i < size; ++i) {
		printf("%d ", vec[i]);
	}
	printf("\n");
}

double get_time()
{
	MPI_Barrier(MPI_COMM_WORLD);
	return omp_get_wtime();
}

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);

	int size;
	int rank;

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	scale = side / SIZE;

	printf("Size: %d\n", size);
	printf("Rank: %d\n", rank);

	int local_size = SIZE / size;
	int local_remain = SIZE % size;

	/* Calculate displacements and sizes for each process*/

	int displs[size];
	int counts[size];

	displs[0] = 0;
	for (int i = 0; i < size; ++i) {
		counts[i] = local_size + (i < local_remain);
		displs[i + 1] = displs[i] + counts[i];
	}

	/* Only allocate full image size to root process */

	int* pic;
	if (rank == 0) {
		pic = calloc(SIZE * SIZE, sizeof(int));
	}
	else {
		pic = calloc(counts[rank] * SIZE, sizeof(int));
	}

	/* Add line datatype */

	MPI_Datatype MATRIX_LINE;
	MPI_Type_contiguous(SIZE, MPI_INT, &MATRIX_LINE);
	MPI_Type_commit(&MATRIX_LINE);

	/* mesure time */

	double time = 0.0f;
	{
		time = get_time();

		mandel_brot(pic, displs[rank], displs[rank] + counts[rank]);

		MPI_Gatherv(pic, counts[rank], MATRIX_LINE, pic, counts, displs, MATRIX_LINE, 0, MPI_COMM_WORLD);

		time = get_time() - time;
	}

	/* Write the full picture in root process */

	if (rank == 0) {
		//print_pic();
		write_pic(pic, "fractal.pgm");
		printf("Time: %f\n", time);
	}

	MPI_Finalize();
	return 0;
}
